import 'package:flutter/material.dart';
import 'body_calender.dart';
import 'NavBar.dart';

class Menu_calender extends StatelessWidget {
  const Menu_calender({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBar(),
      appBar: buildAppBar(),
      body: BodyCalender(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      // Colors: Colors.green,
      elevation: 0,
    );
  }
}

import 'package:flutter/material.dart';

class BodyCalender extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final urlImage = 'assets/images/buu.png';
    final urlImage2 = 'assets/images/Facebook.png';
    final urlImage3 = 'assets/images/pic.png';
    final urlImage4 = 'assets/images/pic2.png';
    final urlImage5 = 'assets/images/pic1.png';
    final urlImage6 = 'assets/images/pic4.png';
    final urlImage7 = 'assets/images/pic5.png';
    final urlImage8 = 'assets/images/pic6.png';
    List<String> num = [
      "10200 : ปริญญาตรี ปกติ ปี 2",
      "1101 : วิทยาลัยนานาชาติชั้นปี 4",
      "2001 : MBA(เข้าศึกษา 2/2560 เป็นต้นไป)",
      "2002 : MBA(เข้าศึกษา 1/2557 ถึง 1/2560)",
      "2003 : MBA(เข้าศึกษาก่อน 1/2557)",
      "10100 : ปริญญาตรี ปกติ ปี 1",
      "10300 : ปริญญาตรี ปกติ ปี 3",
      "10400 : ปริญญาตรี ปกติ ตั้งแต่ ปี 4 ขึ้นไป",
    ];
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        child: Column(
      children: <Widget>[
        Container(
          height: size.height * 0.2,
          child: Stack(
            children: <Widget>[
              Container(
                height: size.height * 0.2 - 27,
                decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(36),
                      bottomRight: Radius.circular(36),
                    ),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 10),
                        blurRadius: 50,
                        color: Color.fromARGB(255, 200, 198, 198),
                      )
                    ]),
              ),
              Positioned(
                left: 40,
                top: 7,
                child: Image.asset(
                  urlImage,
                  width: 90,
                  height: 90,
                ),
              ),
              Positioned(
                  left: 150,
                  top: 7,
                  child: Text('มหาวิทยาลัยบูรพา',
                      style: TextStyle(
                          fontSize: 28.0,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 116, 116, 116)))),
              Positioned(
                  left: 150,
                  top: 50,
                  child: Text('BURAPHA UNIVERSITY',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 255, 255, 255)))),
            ],
          ),
        ),
        Container(
            child: Stack(children: <Widget>[
          Container(
            height: 800,
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 252, 252, 252),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(36),
                  topRight: Radius.circular(36),
                ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 50,
                    color: Color.fromARGB(255, 200, 198, 198),
                  )
                ]),
          ),
          Positioned(
              left: 37,
              top: 19,
              child: Center(
                  child: Text(
                '	ปฏิทินการศึกษา',
                style: TextStyle(
                    color: Color.fromARGB(255, 222, 101, 53),
                    fontWeight: FontWeight.bold,
                    fontSize: 24),
              ))),
          Positioned(
              left: 37,
              top: 75,
              child: Center(
                  child: Text(
                'ชุดปฏิทิน',
                style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0),
                    fontWeight: FontWeight.bold,
                    fontSize: 13),
              ))),
          Padding(
              padding: EdgeInsets.fromLTRB(100, 60, 0, 50),
              // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 16),
              child: DropdownButton(
                value: num[0],
                items: num.map((item) => DropdownMenuItem<String>(
                      value: item,
                      child: Text(
                        item,
                        style: const TextStyle(
                          fontSize: 12,
                        ),
                      ),
                    )).toList(),
                onChanged: (value) {
                  print("You selected $value");
                },
              )),
          Positioned(
              left: 37,
              top: 75,
              child: Center(
                  child: Text(
                'ชุดปฏิทิน',
                style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0),
                    fontWeight: FontWeight.bold,
                    fontSize: 13),
              ))),
        ]))
      ],
    ));
  }
}

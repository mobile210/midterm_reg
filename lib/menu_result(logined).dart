import 'package:flutter/material.dart';
import 'body_result(logined).dart';
import 'NavBar_login.dart';

class MenuResultLogin extends StatelessWidget {
  const MenuResultLogin({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBarLogined(),
      appBar: buildAppBar(),
      body: BodyResultLogined(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      // Colors: Colors.green,
      elevation: 0,
    );
  }
}

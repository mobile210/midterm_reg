
import 'package:flutter/material.dart';
import 'body.dart';
import 'NavBar.dart';

class Menu extends StatelessWidget {
  const Menu({super.key});
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBar(),
      appBar: buildAppBar(),
      body: Body(),
    );
  }
    
  AppBar buildAppBar() {
    return AppBar(
      // Colors: Colors.green,
      elevation: 0,
    );
  }

}


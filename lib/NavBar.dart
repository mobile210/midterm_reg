import 'package:flutter/material.dart';
import 'package:midterm_reg/body.dart';
import 'package:midterm_reg/menu.dart';
import 'package:midterm_reg/menu_login.dart';
import 'package:midterm_reg/menu_sub.dart';
import 'package:midterm_reg/menu_scheduleT.dart';
import 'package:midterm_reg/menu_calender.dart';

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Container(
            height: 50,
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 255, 254, 252),
            ),
          ),
          ListTile(
            leading: Icon(Icons.menu),
            title: Text('เมนูหลัก'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Menu(),
                  ));
            },
          ),
          ListTile(
            leading: Icon(Icons.login),
            title: Text('เข้าสู่ระบบ'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Menu_login(),
                  ));
            },
          ),
          ListTile(
            leading: Icon(Icons.menu_book_sharp),
            title: Text('วิชาที่เปิดสอน'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Menu_sub(),
                  ));
            },
          ),
          ListTile(
            leading: Icon(Icons.table_chart_rounded),
            title: Text('ตารางเรียนนิสิต'),
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                        actions: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text('ปิด')),
                          TextButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Menu_login(),
                                    ));
                              },
                              child: Text('เข้าสู่ระบบ'))
                        ],
                        title: const Text('คำเตือน'),
                        content: const Text(
                            'ระบบไม่อนุญาตให้ท่านใช้งานในส่วนที่ร้องขอ สาเหตุจากท่านยังไม่ได้เข้าสู่ระบบ '),
                      ));
            },
          ),
          ListTile(
            leading: Icon(Icons.table_view_outlined),
            title: Text('ตารางสอนอาจารย์'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Menu_scheduleT(),
                  ));
            },
          ),
          ListTile(
            leading: Icon(Icons.calendar_month),
            title: Text('ปฏิทินการศึกษา'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Menu_calender(),
                  ));
            },
          ),
          ListTile(
            leading: Icon(Icons.book),
            title: Text('หลักสูตรที่เปิดสอน'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.people_alt),
            title: Text('ผู้สำเร็จการศึกษา'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.question_answer_outlined),
            title: Text('ตอบคำถาม'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.help_center_outlined),
            title: Text('แนะนำการลงทะเบียน'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.rule_sharp),
            title: Text('ระเบียบข้อบังคับ'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.receipt_long_rounded),
            title: Text('ค่าธรรมเนียมการศึกษา'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.description_outlined),
            title: Text('Download แบบฟอร์ม'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.bar_chart),
            title: Text('สถิตินิสิต'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.school),
            title: Text('ตรวจสอบวันสำเร็จการศึกษา'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.border_color_outlined),
            title: Text('การยื่นขอสำเร็จการศึกษา'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.download_for_offline),
            title: Text('ดาวน์โหลดปฏิทินการศึกษา'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.phone),
            title: Text('ติดต่อเรา'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.person_pin_outlined),
            title: Text('ข้อมูลรายงานตัวออนไลน์'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.boy),
            title: Text('สำหรับเจ้าหน้าที่'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.account_balance_sharp),
            title: Text('สารสนเทศเจ้าหน้าที่'),
            onTap: () => null,
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'body_logined.dart';
import 'NavBar_login.dart';

class MenuLogin extends StatelessWidget {
  const MenuLogin({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBarLogined(),
      appBar: buildAppBar(),
      body: BodyLogined(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      // Colors: Colors.green,
      elevation: 0,
    );
  }
}

import 'package:flutter/material.dart';

class BodyScheduleT extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final urlImage = 'assets/images/buu.png';
    final urlImage2 = 'assets/images/Facebook.png';
    final urlImage3 = 'assets/images/pic.png';
    final urlImage4 = 'assets/images/pic2.png';
    final urlImage5 = 'assets/images/pic1.png';
    final urlImage6 = 'assets/images/pic4.png';
    final urlImage7 = 'assets/images/pic5.png';
    final urlImage8 = 'assets/images/pic6.png';
    final urlImage9 = 'assets/images/sct1.png';
    final urlImage10 = 'assets/images/sct2.png';
    List<String> num = ["25", "50", "100"];
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        child: Column(
      children: <Widget>[
        Container(
          height: size.height * 0.2,
          child: Stack(
            children: <Widget>[
              Container(
                height: size.height * 0.2 - 27,
                decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(36),
                      bottomRight: Radius.circular(36),
                    ),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 10),
                        blurRadius: 50,
                        color: Color.fromARGB(255, 200, 198, 198),
                      )
                    ]),
              ),
              Positioned(
                left: 40,
                top: 7,
                child: Image.asset(
                  urlImage,
                  width: 90,
                  height: 90,
                ),
              ),
              Positioned(
                  left: 150,
                  top: 7,
                  child: Text('มหาวิทยาลัยบูรพา',
                      style: TextStyle(
                          fontSize: 28.0,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 116, 116, 116)))),
              Positioned(
                  left: 150,
                  top: 50,
                  child: Text('BURAPHA UNIVERSITY',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 255, 255, 255)))),
            ],
          ),
        ),
        Container(
            child: Stack(children: <Widget>[
          Container(
            height: 800,
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 252, 252, 252),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(36),
                  topRight: Radius.circular(36),
                ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 50,
                    color: Color.fromARGB(255, 200, 198, 198),
                  )
                ]),
          ),
          Positioned(
              left: 37,
              top: 19,
              child: Center(
                  child: Text(
                'ตารางสอนอาจารย์',
                style: TextStyle(
                    color: Color.fromARGB(255, 222, 101, 53),
                    fontWeight: FontWeight.bold,
                    fontSize: 24),
              ))),
          Positioned(
              left: 37,
              top: 55,
              child: Center(
                  child: Text(
                'โปรดระบุชื่อท่านอาจารย์',
                style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0),
                    fontWeight: FontWeight.bold,
                    fontSize: 13),
              ))),
          Padding(
              padding: EdgeInsets.fromLTRB(166, 55, 0, 50),
              child: SizedBox(
                width: 130,
                height: 25,
                child: TextField(
                  decoration: InputDecoration(
                    hintText: '',
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 5, horizontal: 25),
                    border: OutlineInputBorder(),
                  ),
                ),
              )),
          Padding(
              padding: EdgeInsets.fromLTRB(305, 55, 0, 50),
              child: ElevatedButton(
                  child: Text("ค้นหา"),
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              actions: [
                                TextButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text('ปิด')),
                              ],
                              content: Image.asset(
                                urlImage9,
                                width: 300,
                                height: 200,
                              ),
                            ));
                  })),
          Positioned(
              left: 37,
              top: 90,
              child: Center(
                  child: Text(
                'จำนวนรายการที่ได้จากการค้นหาไม่เกิน',
                style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0),
                    fontWeight: FontWeight.bold,
                    fontSize: 13),
              ))),
          Padding(
              padding: EdgeInsets.fromLTRB(250, 75, 0, 50),
              // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 16),
              child: DropdownButton(
                value: num[0],
                items: num.map((item) => DropdownMenuItem<String>(
                      value: item,
                      child: Text(
                        item,
                        style: const TextStyle(
                          fontSize: 12,
                        ),
                      ),
                    )).toList(),
                onChanged: (value) {
                  print("You selected $value");
                },
              )),
          Positioned(
              left: 37,
              top: 140,
              child: Center(
                  child: Text(
                'คำแนะนำ',
                style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0),
                    fontWeight: FontWeight.bold,
                    fontSize: 13),
              ))),
          Positioned(
              left: 37,
              top: 170,
              child: Center(
                  child: Text(
                '1.ถ้าต้องการค้นหาอาจารย์ที่มีชื่อขึ้นต้นด้วย สม ให้ป้อน สม*',
                style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0), fontSize: 12),
              ))),
          Positioned(
              left: 37,
              top: 190,
              child: Center(
                  child: Text(
                '2.ถ้าต้องการค้นหาอาจารย์ที่มีชื่อลงท้ายด้วย ชาย ให้ป้อน *ชาย',
                style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0), fontSize: 12),
              ))),
          Positioned(
              left: 37,
              top: 210,
              child: Center(
                  child: Text(
                '3.ระบุจำนวนผลลัพธ์ของรายชื่อที่ต้องการ',
                style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0), fontSize: 12),
              ))),
          Positioned(
              left: 37,
              top: 230,
              child: Center(
                  child: Text(
                '4.กดปุ่มค้นหาเพื่อเริ่มทำการค้นหาตามเงื่อนไข',
                style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0), fontSize: 12),
              ))),
        ]))
      ],
    ));
  }
}

import 'package:flutter/material.dart';
import 'body_scheduleT.dart';
import 'NavBar.dart';

class Menu_scheduleT extends StatelessWidget {
  const Menu_scheduleT({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBar(),
      appBar: buildAppBar(),
      body: BodyScheduleT(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      // Colors: Colors.green,
      elevation: 0,
    );
  }
}

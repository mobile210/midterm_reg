import 'package:flutter/material.dart';
import 'body_sub.dart';
import 'NavBar.dart';

class Menu_sub extends StatelessWidget {
  const Menu_sub({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBar(),
      appBar: buildAppBar(),
      body: BodySub(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      // Colors: Colors.green,
      elevation: 0,
    );
  }
}

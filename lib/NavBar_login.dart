import 'package:flutter/material.dart';
import 'package:midterm_reg/NavBar.dart';
import 'package:midterm_reg/body.dart';
import 'package:midterm_reg/menu.dart';
import 'package:midterm_reg/menu_login.dart';
import 'package:midterm_reg/menu_sub.dart';
import 'package:midterm_reg/menu_scheduleT.dart';
import 'package:midterm_reg/menu_calender.dart';

import 'menu_logined.dart';
import 'menu_profile.dart';
import 'menu_result(logined).dart';
import 'menu_study.dart';

class NavBarLogined extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: const EdgeInsets.all(0),
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.amber,
            ), //BoxDecoration
            child: UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Colors.amber),
              accountName: Text(
                "Mutita Nuangjamnong",
                style: TextStyle(fontSize: 18),
              ),
              accountEmail: Text("63160214@go.buu.ac.th"),
              currentAccountPictureSize: Size.square(50),
              currentAccountPicture: CircleAvatar(
                backgroundColor: Color.fromARGB(255, 251, 255, 137),
                child: Text(
                  "M",
                  style: TextStyle(
                      fontSize: 30.0, color: Color.fromARGB(255, 0, 0, 0)),
                ), //Text
              ), //circleAvatar
            ), //UserAccountDrawerHeader
          ),
          ListTile(
            leading: const Icon(Icons.menu),
            title: const Text('หน้าหลัก'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MenuLogin(),
                  ));
            },
          ), //DrawerHeader
          ListTile(
            leading: const Icon(Icons.person),
            title: const Text('ประวัตินิสิต'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MenuProfile(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.calendar_month),
            title: const Text('ตารางเรียน/สอบ'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MenuStudy(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.book),
            title: const Text('ผลการศึกษา'),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: const Icon(Icons.app_registration_rounded),
            title: const Text('ลงทะเบียน'),
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                        actions: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text('ตกลง')),
                        ],
                        title: const Text('reg buu'),
                        content: const Text(
                            'นิสิตทำการลงทะเบียนแล้วโปรดตรวจสอบรายวิชาที่เมนูผลการลงทะเบียน'),
                      ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.score),
            title: const Text('ผลลงทะเบียน'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MenuResultLogin(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.logout),
            title: const Text('Logout'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Menu(),
                  ));
            },
          ),
        ],
      ),
    );
  }
}

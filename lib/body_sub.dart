import 'package:flutter/material.dart';

class BodySub extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final urlImage = 'assets/images/buu.png';
    final urlImage2 = 'assets/images/Facebook.png';
    final urlImage3 = 'assets/images/pic.png';
    final urlImage4 = 'assets/images/pic2.png';
    final urlImage5 = 'assets/images/pic1.png';
    final urlImage6 = 'assets/images/pic4.png';
    final urlImage7 = 'assets/images/pic5.png';
    final urlImage8 = 'assets/images/pic6.png';
    List<String> sub = [
      "รหัสวิชาที่ยังใช้อยู่",
      "ทุกรหัสวิชา",
      "รหัสวิชาที่เลิกใช้แล้ว"
    ];
    List<String> sub1 = [
      "ทั้งหมด",
      "คณะพยาบาลศาสตร์",
      "คณะมนุษยศาสตร์และสังคมศาสตร์",
      "คณะวิทยาศาสตร์",
      "คณะศึกษาศาสตร์",
      "คณะวิศวกรรมศาสตร์",
      "คณะศิลปกรรมศาสตร์",
      "คณะสาธารณสุขศาสตร์",
      "คณะเทคโนโลยีทางทะเล",
      "วิทยาลัยการพาณิชยนาวี",
      "วิทยาลัยอัญมณี",
      "คณะวิทยาศาสตร์และศิลปศาสตร์",
      "วิทยาลัยการบริหารรัฐกิจ",
      "วิทยาลัยพาณิชยศาสตร์",
      "คณะวิทยาศาสตร์การกีฬา",
      "วิทยาลัยการขนส่งและโลจิสติกส์",
      "วิทยาลัยวิทยาศาสตร์การกีฬา",
      "วิทยาลัยนานาชาติ",
      "คณะแพทยศาสตร์",
      "คณะรัฐศาสตร์และนิติศาสตร์",
      "คณะการจัดการและการท่องเที่ยว",
      "คณะโลจิสติกส์",
      "วิทยาลัยวิทยาการวิจัยและวิทยาการปัญญา",
      "คณะอัญมณี",
      "คณะวิทยาศาสตร์และสังคมศาสตร์",
      "คณะเทคโนโลยีการเกษตร",
      "คณะการแพทย์แผนไทยอภัยภูเบศร",
      "บัณฑิตวิทยาลัย",
      "คณะสหเวชศาสตร์",
      "คณะเภสัชศาสตร์",
      "คณะภูมิสารสนเทศศาสตร์",
      "คณะวิทยาการสารสนเทศ",
      "คณะดนตรีและการแสดง",
      "โครงการจัดตั้งคณะพาณิชยศาสตร์และบริหารธุรกิจ",
      "คณะบริหารธุรกิจ",
      "สถาบันภาษา",
    ];
    List<String> sub2 = ["25", "50", "100", "250"];
    List<String> sub3 = [
      "ทั้งหมด",
      "1 : บางแสน",
      "2 : จันทบุรี",
      "3 : สระแก้ว",
      "80 : สถาบันสมทบ",
    ];
    List<String> sub4 = [
      "ทั้งหมด",
      "0 : -ไม่กำหนด-",
      "1 : ปริญญาตรี ปกติ",
      "2 : ปริญญาตรี พิเศษ",
      "4 : บัณฑิตศึกษา ปกติ",
      "5 : บัณฑิตศึกษา พิเศษ",
      "6 : ประกาศนียบัตร (ต่ำกว่า ป.ตรี)",
      "7 : อนุปริญญา",
    ];
    List<String> sub5 = [
      "ทั้งหมด",
      "X : -ยังไม่กำหนด-",
      "W : วิชาบังคับ",
      "M : วิชาพื้นฐาน",
      "G : วิชาศึกษาทั่วไป",
      "F : วิชาเสรีเลือก",
      "E : วิชาเลือกเฉพาะสาขา",
      "B : วิชาเสริมพื้นฐาน",
    ];
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        child: Column(
      children: <Widget>[
        Container(
          height: size.height * 0.2,
          child: Stack(
            children: <Widget>[
              Container(
                height: size.height * 0.2 - 27,
                decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(36),
                      bottomRight: Radius.circular(36),
                    ),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 10),
                        blurRadius: 50,
                        color: Color.fromARGB(255, 200, 198, 198),
                      )
                    ]),
              ),
              Positioned(
                left: 40,
                top: 7,
                child: Image.asset(
                  urlImage,
                  width: 90,
                  height: 90,
                ),
              ),
              Positioned(
                  left: 150,
                  top: 7,
                  child: Text('มหาวิทยาลัยบูรพา',
                      style: TextStyle(
                          fontSize: 28.0,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 116, 116, 116)))),
              Positioned(
                  left: 150,
                  top: 50,
                  child: Text('BURAPHA UNIVERSITY',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 255, 255, 255)))),
            ],
          ),
        ),
        Container(
            child: Stack(children: <Widget>[
          Container(
            height: 680,
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 252, 252, 252),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(36),
                  topRight: Radius.circular(36),
                ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 50,
                    color: Color.fromARGB(255, 200, 198, 198),
                  )
                ]),
          ),
          Positioned(
              left: 37,
              top: 19,
              child: Center(
                  child: Text(
                'วิชาที่เปิดสอน',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 24),
              ))),
          Positioned(
              left: 37,
              top: 67,
              child: Center(
                  child: Text(
                'ขั้นที่ 1 หมวดวิชา',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ))),
          Padding(
              padding: EdgeInsets.fromLTRB(37, 85, 0, 50),
              // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 16),
              child: DropdownButton(
                value: sub[0],
                items: sub
                    .map((item) => DropdownMenuItem<String>(
                          value: item,
                          child: Text(
                            item,
                            style: const TextStyle(
                              fontSize: 14,
                            ),
                          ),
                        ))
                    .toList(),
                onChanged: (value) {
                  print("You selected $value");
                },
              )),
          Positioned(
              left: 37,
              top: 140,
              child: Center(
                  child: Text(
                'ขั้นที่ 2 หน่วยงานเจ้าของรายวิชา',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ))),
          Padding(
              padding: EdgeInsets.fromLTRB(37, 158, 0, 50),
              // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 16),
              child: DropdownButton(
                value: sub1[0],
                items: sub1
                    .map((item) => DropdownMenuItem<String>(
                          value: item,
                          child: Text(
                            item,
                            style: const TextStyle(
                              fontSize: 14,
                            ),
                          ),
                        ))
                    .toList(),
                onChanged: (value) {
                  print("You selected $value");
                },
              )),
          Positioned(
              left: 37,
              top: 213,
              child: Center(
                  child: Text(
                'ขั้นที่ 3 จำนวนรายการที่ได้จากการค้นหาไม่เกิน',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ))),
          Padding(
              padding: EdgeInsets.fromLTRB(37, 231, 0, 50),
              // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 16),
              child: DropdownButton(
                value: sub2[0],
                items: sub2
                    .map((item) => DropdownMenuItem<String>(
                          value: item,
                          child: Text(
                            item,
                            style: const TextStyle(
                              fontSize: 14,
                            ),
                          ),
                        ))
                    .toList(),
                onChanged: (value) {
                  print("You selected $value");
                },
              )),
          Positioned(
              left: 37,
              top: 286,
              child: Center(
                  child: Text(
                'ปีการศึกษา2565  / 1 2 ฤดูร้อน ',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ))),
          Positioned(
              left: 37,
              top: 306,
              child: Center(
                  child: Text(
                'ขั้นที่ 4	ปีการศึกษา ',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ))),
          Positioned(
              left: 79,
              top: 326,
              child: Center(
                  child: Text(
                'วิทยาเขต ',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ))),
          Padding(
              padding: EdgeInsets.fromLTRB(168, 310, 0, 50),
              // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 16),
              child: DropdownButton(
                value: sub3[0],
                items: sub3
                    .map((item) => DropdownMenuItem<String>(
                          value: item,
                          child: Text(
                            item,
                            style: const TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ))
                    .toList(),
                onChanged: (value) {
                  print("You selected $value");
                },
              )),
          Positioned(
              left: 79,
              top: 354,
              child: Center(
                  child: Text(
                'ระดับการศึกษา',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ))),
          Padding(
              padding: EdgeInsets.fromLTRB(175, 340, 0, 50),
              // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 16),
              child: DropdownButton(
                value: sub4[0],
                items: sub4
                    .map((item) => DropdownMenuItem<String>(
                          value: item,
                          child: Text(
                            item,
                            style: const TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ))
                    .toList(),
                onChanged: (value) {
                  print("You selected $value");
                },
              )),
          Positioned(
              left: 79,
              top: 382,
              child: Center(
                  child: Text(
                'ประเภท',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ))),
          Padding(
              padding: EdgeInsets.fromLTRB(155, 368, 0, 50),
              // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 16),
              child: DropdownButton(
                value: sub5[0],
                items: sub5
                    .map((item) => DropdownMenuItem<String>(
                          value: item,
                          child: Text(
                            item,
                            style: const TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ))
                    .toList(),
                onChanged: (value) {
                  print("You selected $value");
                },
              )),
          Positioned(
              left: 37,
              top: 410,
              child: Center(
                  child: Text(
                'ขั้นที่ 5',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ))),
          Positioned(
              left: 37,
              top: 430,
              child: Center(
                  child: Text(
                'ป้อนข้อความลงในช่องรหัสวิชาและ/หรือชื่อวิชาแล้วกดปุ่ม',
                style: TextStyle(color: Colors.black, fontSize: 15),
              ))),
          Positioned(
              left: 37,
              top: 450,
              child: Center(
                  child: Text(
                'ค้นหาเพื่อเริ่มทำการค้นหาตามเงื่อนไข',
                style: TextStyle(color: Colors.black, fontSize: 15),
              ))),
          Padding(
            // padding: EdgeInsets.all(20),
            padding: EdgeInsets.fromLTRB(37, 475, 240, 0),
            // padding: EdgeInsets.only(left: 37, top: 90),
            // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 16),
            child: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                contentPadding: EdgeInsets.symmetric(vertical: 5.0),
                hintText: '   *',
              ),
            ),
          ),
          Padding(
            // padding: EdgeInsets.all(20),
            padding: EdgeInsets.fromLTRB(160, 475, 90, 0),
            // padding: EdgeInsets.only(left: 37, top: 90),
            // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 16),
            child: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                contentPadding: EdgeInsets.symmetric(vertical: 5.0),
                hintText: ' ',
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.fromLTRB(305, 485, 0, 50),
              child: ElevatedButton(child: Text("ค้นหา"), onPressed: () {})),
          Positioned(
              left: 37,
              top: 525,
              child: Center(
                  child: Text(
                '*** กรุณาระบุรหัสวิชา หรือชื่อวิชาเพื่อใช้ในการค้นหา ***',
                style: TextStyle(
                    color: Color.fromARGB(255, 117, 6, 6), fontSize: 12),
              ))),
        ]))
      ],
    ));
  }
}

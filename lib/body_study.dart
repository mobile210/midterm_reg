import 'dart:js_util';
import 'package:flutter/material.dart';

class BodyStudy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final urlImage = 'assets/images/buu.png';
    final urlImage2 = 'assets/images/Facebook.png';
    final urlImage3 = 'assets/images/pic.png';
    final urlImage4 = 'assets/images/pic2.png';
    final urlImage5 = 'assets/images/pic1.png';
    final urlImage6 = 'assets/images/pic4.png';
    final urlImage7 = 'assets/images/pic5.png';
    final urlImage8 = 'assets/images/pic6.png';
    final study = 'assets/images/study.png';
    final picfinal = 'assets/images/picfinal.png';
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        child: Column(
      children: <Widget>[
        Container(
          height: size.height * 0.2,
          child: Stack(
            children: <Widget>[
              Container(
                height: size.height * 0.2 - 27,
                decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(36),
                      bottomRight: Radius.circular(36),
                    ),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 10),
                        blurRadius: 50,
                        color: Color.fromARGB(255, 200, 198, 198),
                      )
                    ]),
              ),
              Positioned(
                left: 40,
                top: 7,
                child: Image.asset(
                  urlImage,
                  width: 90,
                  height: 90,
                ),
              ),
              Positioned(
                  left: 150,
                  top: 7,
                  child: Text('มหาวิทยาลัยบูรพา',
                      style: TextStyle(
                          fontSize: 28.0,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 116, 116, 116)))),
              Positioned(
                  left: 150,
                  top: 50,
                  child: Text('BURAPHA UNIVERSITY',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 255, 255, 255)))),
            ],
          ),
        ),
        Container(
            child: Stack(children: <Widget>[
          Container(
            height: 680,
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 252, 252, 252),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(36),
                  topRight: Radius.circular(36),
                ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 50,
                    color: Color.fromARGB(255, 200, 198, 198),
                  )
                ]),
          ),
          Positioned(
              left: 37,
              top: 27,
              child: Center(
                  child: Text(
                '63160214 : นางสาวมุฑิตา เนื่องจำนงค์ : คณะวิทยาการสารสนเทศ',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ))),
          Positioned(
              left: 37,
              top: 47,
              child: Center(
                  child: Text(
                'หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์)',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ))),
          Positioned(
              left: 37,
              top: 67,
              child: Center(
                  child: Text(
                'ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ))),
          Positioned(
              left: 37,
              top: 87,
              child: Center(
                  child: Text(
                'สถานภาพ: กำลังศึกษา',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ))),
          Positioned(
              left: 37,
              top: 107,
              child: Center(
                  child: Text(
                'อ. ที่ปรึกษา: อาจารย์ภูสิต กุลเกษม,ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ))),
          // Positioned(
          //     left: 37,
          //     top: 207,
          //     child: Container(
          //       decoration: BoxDecoration(
          //         borderRadius: BorderRadius.circular(12),
          //         color: Colors.white,
          //         boxShadow: [
          //           BoxShadow(
          //             color: Colors.grey.withOpacity(0.5),
          //             spreadRadius: 5,
          //             blurRadius: 7,
          //             offset: Offset(0, 2),
          //           ),
          //         ],
          //       ),
          //       child: Column(
          //         children: [
          //           Container(
          //             child: Icon(Icons.person,
          //                 size: 24, color: Colors.blueAccent),
          //             padding: const EdgeInsets.all(24),
          //           ),
          //           Container(
          //             decoration: const BoxDecoration(
          //                 color: Colors.blueAccent,
          //                 borderRadius: BorderRadius.only(
          //                     bottomRight: Radius.circular(12),
          //                     bottomLeft: Radius.circular(12))),
          //             child: Text("Student"),
          //             padding: const EdgeInsets.all(24),
          //           )
          //         ],
          //       ),
          //     )),
          Positioned(
              top: 140,
              child: Center(
                child: Container(
                    width: 500,
                    decoration: const BoxDecoration(
                      color: Color.fromARGB(255, 251, 210, 85),
                    ),
                    child: Text(
                      '        ',
                      style: TextStyle(
                          color: Color.fromARGB(255, 6, 6, 6), fontSize: 3),
                    )),
              )),
          Positioned(
              left: 18,
              top: 160,
              child: Center(
                  child: Text(
                'ตารางเรียนของรายวิชาที่ลงทะเบียนไว้แล้ว',
                style: TextStyle(
                    color: Color.fromARGB(255, 246, 137, 3),
                    fontWeight: FontWeight.bold,
                    fontSize: 22),
              ))),

          Positioned(
              left: 18,
              top: 190,
              child: Center(
                  child: Text(
                'ปีการศึกษา2565  / 1 2 ฤดูร้อน ระหว่าง  30/1/2566 - 5/2/2566 ',
                style: TextStyle(
                    color: Color.fromARGB(255, 6, 6, 6), fontSize: 12),
              ))),

          Positioned(
            left: 18,
            top: 150,
            child: Image.asset(
              study,
              width: 360,
              height: 280,
            ),
          ),
        ]))
      ],
    ));
  }
}

import 'dart:js_util';
import 'package:flutter/material.dart';

class BodyLogined extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final urlImage = 'assets/images/buu.png';
    final urlImage2 = 'assets/images/Facebook.png';
    final urlImage3 = 'assets/images/pic.png';
    final urlImage4 = 'assets/images/pic2.png';
    final urlImage5 = 'assets/images/pic1.png';
    final urlImage6 = 'assets/images/pic4.png';
    final urlImage7 = 'assets/images/pic5.png';
    final urlImage8 = 'assets/images/pic6.png';
    final card = 'assets/images/card.png';
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        child: Column(
      children: <Widget>[
        Container(
          height: size.height * 0.2,
          child: Stack(
            children: <Widget>[
              Container(
                height: size.height * 0.2 - 27,
                decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(36),
                      bottomRight: Radius.circular(36),
                    ),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 10),
                        blurRadius: 50,
                        color: Color.fromARGB(255, 200, 198, 198),
                      )
                    ]),
              ),
              Positioned(
                left: 40,
                top: 7,
                child: Image.asset(
                  urlImage,
                  width: 90,
                  height: 90,
                ),
              ),
              Positioned(
                  left: 150,
                  top: 7,
                  child: Text('มหาวิทยาลัยบูรพา',
                      style: TextStyle(
                          fontSize: 28.0,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 116, 116, 116)))),
              Positioned(
                  left: 150,
                  top: 50,
                  child: Text('BURAPHA UNIVERSITY',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 255, 255, 255)))),
            ],
          ),
        ),
        Container(
            child: Stack(children: <Widget>[
          Container(
            height: 800,
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 252, 252, 252),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(36),
                  topRight: Radius.circular(36),
                ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 50,
                    color: Color.fromARGB(255, 200, 198, 198),
                  )
                ]),
          ),
          Positioned(
              left: 37,
              top: 27,
              child: Center(
                  child: Text(
                '63160214 : นางสาวมุฑิตา เนื่องจำนงค์ : คณะวิทยาการสารสนเทศ',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ))),
          Positioned(
              left: 37,
              top: 47,
              child: Center(
                  child: Text(
                'หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์)',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ))),
          Positioned(
              left: 37,
              top: 67,
              child: Center(
                  child: Text(
                'ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ))),
          Positioned(
              left: 37,
              top: 87,
              child: Center(
                  child: Text(
                'สถานภาพ: กำลังศึกษา',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ))),
          Positioned(
              left: 37,
              top: 107,
              child: Center(
                  child: Text(
                'อ. ที่ปรึกษา: อาจารย์ภูสิต กุลเกษม,ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ))),
          // Positioned(
          //     left: 37,
          //     top: 207,
          //     child: Container(
          //       decoration: BoxDecoration(
          //         borderRadius: BorderRadius.circular(12),
          //         color: Colors.white,
          //         boxShadow: [
          //           BoxShadow(
          //             color: Colors.grey.withOpacity(0.5),
          //             spreadRadius: 5,
          //             blurRadius: 7,
          //             offset: Offset(0, 2),
          //           ),
          //         ],
          //       ),
          //       child: Column(
          //         children: [
          //           Container(
          //             child: Icon(Icons.person,
          //                 size: 24, color: Colors.blueAccent),
          //             padding: const EdgeInsets.all(24),
          //           ),
          //           Container(
          //             decoration: const BoxDecoration(
          //                 color: Colors.blueAccent,
          //                 borderRadius: BorderRadius.only(
          //                     bottomRight: Radius.circular(12),
          //                     bottomLeft: Radius.circular(12))),
          //             child: Text("Student"),
          //             padding: const EdgeInsets.all(24),
          //           )
          //         ],
          //       ),
          //     )),
          Positioned(
              left: 27,
              top: 140,
              child: Container(
                width: 340,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Color.fromARGB(255, 218, 226, 241),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 7,
                      offset: Offset(0, 2),
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Container(
                      child: Text("คะแนนภาษาอังกฤษครั้งแรก 35 คะแนน"),
                      padding: const EdgeInsets.all(3),
                    ),
                    Container(
                      child: Text("คะแนนภาษาอังกฤษครั้งสอง - คะแนน"),
                      padding: const EdgeInsets.all(3),
                    ),
                    Container(
                      decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 255, 255, 255),
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(12),
                              bottomLeft: Radius.circular(12))),
                      child: Text(
                          "สำหรับนิสิตตั้งแต่รหัส59 เป็นต้นไปต้องสอบให้ครบ 2 ครั้ง จึงจะสำเร็จการศึกษาได้"),
                      padding: const EdgeInsets.all(3),
                    ),
                  ],
                ),
              )),
          Positioned(
              top: 270,
              child: Center(
                child: Container(
                  width: 500,
                  decoration: const BoxDecoration(
                    color: Colors.amber,
                  ),
                  child: Text(
                    "ประกาศเรื่อง",
                    style: TextStyle(
                        color: Color.fromARGB(255, 255, 255, 255),
                        fontWeight: FontWeight.bold,
                        fontSize: 12),
                  ),
                ),
              )),
          Positioned(
              left: 37,
              top: 290,
              child: Center(
                  child: Text(
                '1.	แบบประเมินความคิดเห็นของนักเรียนและนิสิตต่อการให้บริการ',
                style: TextStyle(
                    color: Color.fromARGB(255, 9, 3, 84),
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ))),
          Positioned(
              left: 37,
              top: 310,
              child: Center(
                  child: Text(
                'ของสำนักงานอธิการบดี(ด่วนที่สุด)',
                style: TextStyle(
                    color: Color.fromARGB(255, 9, 3, 84),
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ))),
          Positioned(
              left: 27,
              top: 330,
              child: Container(
                width: 340,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  color: Color.fromARGB(255, 179, 224, 249),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 7,
                      offset: Offset(0, 2),
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Container(
                      child: Text(
                          "ขอเชิญนิสิตร่วมทำแบบประเมินความคิดเห็นของนิสิตต่อการให้บริการของสำนักงานอธิการบดี ที่ https://bit.ly/3cyvuuf"),
                      padding: const EdgeInsets.all(3),
                    ),
                  ],
                ),
              )),
          Positioned(
              left: 37,
              top: 390,
              child: Center(
                  child: Text(
                '2.	การทำบัตรนิสิตกับธนาคารกรุงไทย',
                style: TextStyle(
                    color: Color.fromARGB(255, 9, 3, 84),
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              ))),
          Positioned(
            left: 25,
            top: 420,
            child: Image.asset(
              card,
              width: 350,
              height: 350,
            ),
          ),
        ]))
      ],
    ));
  }
}

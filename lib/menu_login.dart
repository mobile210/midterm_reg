import 'package:flutter/material.dart';
import 'body_login.dart';
import 'NavBar.dart';

class Menu_login extends StatelessWidget {
  const Menu_login({super.key});
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBar(),
      appBar: buildAppBar(),
      body: BodyLogin(),
    );
  }
    
  AppBar buildAppBar() {
    return AppBar(
      // Colors: Colors.green,
      elevation: 0,
    );
  }

}


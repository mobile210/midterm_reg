import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final urlImage = 'assets/images/buu.png';
    final urlImage2 = 'assets/images/Facebook.png';
    final urlImage3 = 'assets/images/pic.png';
    final urlImage4 = 'assets/images/pic2.png';
    final urlImage5 = 'assets/images/pic1.png';
    final urlImage6 = 'assets/images/pic4.png';
    final urlImage7 = 'assets/images/pic5.png';
    final urlImage8 = 'assets/images/pic6.png';
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
    child: Column(
      children: <Widget>[
        Container(
          height: size.height * 0.2,
          child: Stack(
            children: <Widget>[
              Container(
                height: size.height * 0.2 - 27,
                decoration: BoxDecoration(
                  color: Colors.amber,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(36),
                    bottomRight: Radius.circular(36),
                  ),
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0,10),
                      blurRadius:  50,
                      color: Color.fromARGB(255, 200, 198, 198),
                    )
                  ]
                ),
              ),
              Positioned(
                left: 40,
                top: 7,
                child: Image.asset(
                  urlImage,
                  width: 90,
                  height: 90,
                ),
              ),
              Positioned(
                  left: 150,
                  top: 7,
                  child: Text('มหาวิทยาลัยบูรพา',
                      style: TextStyle(
                          fontSize: 28.0,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 116, 116, 116)))
              ),
              Positioned(
                  left: 150,
                  top: 50,
                  child: Text('BURAPHA UNIVERSITY',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 255, 255, 255)))),
              
            ],
          ),
        ),
        Container(
          child: Stack(
            children: <Widget>[
              Container(
                height: 2000,
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 252, 252, 252),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(36),
                    topRight: Radius.circular(36),
                  ),
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0,10),
                      blurRadius:  50,
                      color: Color.fromARGB(255, 200, 198, 198),
                    )
                  ]
                ),
              ),
              
              Positioned(
                  left: 37,
                  top: 20,
                  child: Container(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size(30, 30),
                        textStyle: TextStyle(fontSize: 10),
                      ),
                      child: Text('เกี่ยวกับทะเบียนฯ') ,
                      onPressed: () {},
                      )
                  )
                  ),
                  Positioned(
                  left: 150,
                  top: 20,
                  child: Container(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size(30, 30),
                        textStyle: TextStyle(fontSize: 10),
                      ),
                      child: Text('แนะนำการลงทะเบียน') ,
                      onPressed: () {},
                      )
                  )
                  ),
                  Positioned(
                  left: 280,
                  top: 16,
                  child: Image.asset(
                  urlImage2,
                  width: 30,
                  height: 30,
                ),
                  ),
                  Positioned(
                  left: 37,
                  top: 50,
                  child: Text('ประกาศเรื่อง',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 0, 0, 0),
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 70,
                  child: Text('1.การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),)
                  ),
                  Positioned(
                  left: 340,
                  top: 70,
                  child: Text('(ด่วน)',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 254, 21, 21),
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),)
                  ),
                  
                  Positioned(
                  left: 25,
                  top: 35,
                  child: Image.asset(
                  urlImage3,
                  width: 350,
                  height: 350,
                ),
                  ),
                  Positioned(
                  left: 37,
                  top: 345,
                  child: Text('**นิสิตต้องยื่นคำร้องขอสำเร็จการศึกษาและชำระเงินตามกำหนดเวลา',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 254, 21, 21),
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 356,
                  child: Text('หากนิสิตไม่ได้ยื่นสำเร็จการศึกษาทางกองทะบียนฯ จะไม่เสนอชื่อสำเร็จการศึกษา',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 254, 21, 21),
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 367,
                  child: Text('คู่มือการยื่นคำร้องสำเร็จการศึกษา นิสิตพิมพ์ใบชำระเงินที่เมนูแจ้งจบและขึ้นทะเบียนนิสิต',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 0, 0, 0),
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 390,
                  child: Text('นิสิตสิตจะรับเอกสารจบการศึกษาได้ต่อเมื่อนิสิตมีสถานะจบการศึกษา',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 0, 0, 0),
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 400,
                  child: Text('โดยตรวจสอบที่เมนูตรวจสอบวันที่สำเร็จการศึกษา',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 0, 0, 0),
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 430,
                  child: Text('2.กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 445,
                  child: Text('ปีการศึกษา 2565',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),)
                  ),
                  Positioned(
                  left: 25,
                  top: 370,
                  child: Image.asset(
                  urlImage4,
                  width: 350,
                  height: 350,
                ),
                  ),
                  Positioned(
                  left: 37,
                  top: 630,
                  child: Text('สำหรับการ scan ชำระค่าธรรมเนียมการศึกษา แอปธนาคารกสิกรไทยไม่สามารถชำระได้',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 255, 8, 41),
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 643,
                  child: Text('ให้ใช้แอปของธนาคารอื่นๆ ขออภัยในความไม่สะดวกครับ',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 255, 8, 41),
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 670,
                  child: Text('3.กำหนดยื่นคำร้องเกี่ยวกับงานพิธีพระราชทานปริญญาบัตร',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 685,
                  child: Text('ปีการศึกษา 2563 และปีการศึกษา 2564',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),)
                  ),
                  Positioned(
                  left: 25,
                  top: 655,
                  child: Image.asset(
                  urlImage5,
                  width: 350,
                  height: 350,
                ),
                  ),
                  Positioned(
                  left: 37,
                  top: 960,
                  child: Text('4.แบบประเมินความคิดเห็นต่อการให้บริการของสำนักงานอธิการบดี',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 980,
                  child: Text('ขอเชิญนิสิตและผู้ปกครองร่วมทำแบบประเมินความคิดเห็นต่อการให้บริการของ',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 993,
                  child: Text('สำนักงานอธิการบดี',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 1015,
                  child: Text('-  สำหรับนิสิต ที่ https://bit.ly/3cyvuuf',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                   Positioned(
                  left: 37,
                  top: 1027,
                  child: Text('-  สำหรับผู้ปกครอง ที่ https://bit.ly/39uIUWa',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                  Positioned(
                  left: 25,
                  top: 977,
                  child: Image.asset(
                  urlImage6,
                  width: 350,
                  height: 350,
                ),
                  ),
                  Positioned(
                  left: 37,
                  top: 1270,
                  child: Text('5.LINE Official',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 1285,
                  child: Text('เพิ่มเพื่อน LINE Official ของกองทะเบียนฯ พิมพ์ "@regbuu"',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                  Positioned(
                  left: 37,
                  top: 1300,
                  child: Text('(ใส่ @ ด้วย) หรือ https://lin.ee/uNohJQP หรือสแกน Qrcode ด้านล่าง',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 10,
                      fontWeight: FontWeight.w500,
                    ),)
                  ),
                  Positioned(
                  left: 25,
                  top: 1250,
                  child: Image.asset(
                  urlImage7,
                  width: 350,
                  height: 350,
                ),
                  ),
                  Positioned(
                  left: 37,
                  top: 1560,
                  child: Text('6.Download ระเบียบสำหรับแนบเบิกค่าเล่าเรียนบุตร',
                  style:const TextStyle(
                      color: Color.fromARGB(255, 14, 2, 75),
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),)
                  ),
                  Positioned(
                  left: 25,
                  top: 1460,
                  child: Image.asset(
                  urlImage8,
                  width: 350,
                  height: 350,
                ),
                  ),
                  
                 
            ]
          )
          )
        
      ],
    )
    );
  }
}

import 'package:flutter/material.dart';
import 'body_profile.dart';
import 'NavBar_login.dart';

class MenuProfile extends StatelessWidget {
  const MenuProfile({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBarLogined(),
      appBar: buildAppBar(),
      body: BodyProfile(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      // Colors: Colors.green,
      elevation: 0,
    );
  }
}

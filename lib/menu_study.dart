import 'package:flutter/material.dart';
import 'body_study.dart';
import 'NavBar_login.dart';

class MenuStudy extends StatelessWidget {
  const MenuStudy({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBarLogined(),
      appBar: buildAppBar(),
      body: BodyStudy(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      // Colors: Colors.green,
      elevation: 0,
    );
  }
}
